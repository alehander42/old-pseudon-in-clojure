(ns pseudon.python_annotations)

(def python-annotations {
    :String {
        :split        [:string/split 0 1]
        :join         [:list/join 1 0]
    }
    :List {
        :append       [:list/push 0 1]
        :extend       [:list/extend 0 1]
    }
    :Any {
        :__len__      [:any/length 0]
        :__str__      [:any/to-string 0]
    }
    :Dict {
        :keys         [:dict/keys 0]
        :values       [:dict/values 0]
    }
})
