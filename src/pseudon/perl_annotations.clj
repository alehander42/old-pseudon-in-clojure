(ns pseudon.perl_annotations)

(def perl-annotations {
    :String {
        :split        [:string/split 1 0]
        :.            [:string/concat 0 1]
    }
    :List {
        :push         [:list/push 0 1]
        :shift        [:list/shift 0]
    }
    :Any {
        :scalar       [:any/length 0]
        :sprintf      [:any/to-string 0]
    }
    :Dict {
        :keys         [:dict/keys 0]
        :values       [:dict/values 0]
    }
})
