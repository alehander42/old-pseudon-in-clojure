(ns pseudon.ruby_emitter
  (use [pseudon.code_formatter]
       [pseudon.node_types]
       [pseudon.lang_emitter])
  (:require pseudon.node_types)
  (:import [pseudon.node_types Cell PsClass Def PsName PsName4 PsInstanceName Method InstanceAssignment Assignment Args If Parent ImplicitReturn ExplicitReturn ConditionBody PsString PsInteger]))
" emits ruby code"

(defn format-ruby
  [code]
  (code-formatter code "  " {
    :after {"def" :indent "class" :indent "module" :indent "if" :indent "elsif" :indent "else" :indent}
    :after-end {"do" :indent}
    :before {"end" :dedent}
  }))

(defmulti emit-ruby class)

(defmethod emit-ruby :default [x] (str x))

(emitter emit-ruby
  Cell                [[:z "\n"]]

  PsName              [:name]

  nil                 ""

  PsInstanceName      ["@" :name]

  Assignment          [:target " = " :right]

  InstanceAssignment  [:target " = " :right]

  Def                 ["def " :name :args :NL
                       [:body "\n" 1] :NL
                       "end" :NL]

  Method              ["def " :name :args :NL
                       [:body "\n" 1] :NL
                       "end" :NL]

  Args                ["(" [:args ", "] ")"]

  PsClass             ["class " :name :parent :NL
                       [:body "\n" 1] :NL
                      "end" :NL]

  PsName4             [:name]

  Parent              [" < " :object]

  ImplicitReturn      [:object]

  ExplicitReturn      ["return " :object]

  PsInteger             [:value]

  PsString              ["\"" :value "\""])
