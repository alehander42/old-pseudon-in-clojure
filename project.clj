(defproject pseudon "0.2.0-pseudon"
  :description "pseudon compiler"
  :url "https://github.com/alehander42/pseudon"
  :license {:name "MIT"
            :url "http://www.opensource.org/licenses/mit-license.php"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [defun "0.3.0-alapha"]
                 [org.clojure/core.match "0.3.0-alpha4"]]
  :main ^:skip-aot pseudon.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
